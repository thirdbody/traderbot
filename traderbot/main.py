#! /usr/bin/env python3

# Where execution begins.

import os                                                # for checking current working dir
import os.path                                           # for checking if preferences exist
from traderbot.setup.first_time import first_time_setup  # First time app setup
from traderbot.setup.load_prefs import load_prefs        # Loading preferences
from time import sleep                                   # Sleeping 5 min
from homedir import get_home_dir                         # For locating preferences

def main():
    """Main function, handling the execution of the program."""

    if not os.path.exists(get_home_dir() + ".trader-prefs"):  # Checks home/User directory for prefs file
        first_time_setup()  # Generate prefs file from user input

    try:
        broker, trader = load_prefs()  # Loads data on broker (exchange) and trader from the prefs file.
    except IndexError:
        print("There was an issue formatting the preferences file at '" + get_home_dir() + ".trader_prefs'.")
        print("Remove the file and try again.")
        exit(1)  # Exits with failure code

    iterations = 0
    while True:

        if trader.past_max():  # If price of currency is past the max set in prefs file
            trader.sell()
        elif trader.past_min():  # If price of currency is past the minimum set in prefs file
            trader.buy()

        print("As of Iteration", iterations, "total profit is", trader.total_profit)
        sleep(300)  # 5 minutes
        iterations += 1


main()
