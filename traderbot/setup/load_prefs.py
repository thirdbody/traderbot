"""Reads data from the prefs file and makes objects from it."""

from traderbot.exchanges.coinbase_broker import Coinbase
from traderbot.traders.usd_trader import USD_Trader
from homedir import get_home_dir

def load_prefs():
    """Reads data from the prefs file and makes objects from it."""

    prefs = open(get_home_dir() + ".trader-prefs", 'r')

    # Remember: order matters!

    broker = get_value_from_prefs(prefs, "Broker")

    api_key = get_value_from_prefs(prefs, "Key")
    secret_key = get_value_from_prefs(prefs, "Secret")

    currency = get_value_from_prefs(prefs, "Currency")

    usd_per_buy = get_value_from_prefs(prefs, "USD_Per_Buy")

    low = get_value_from_prefs(prefs, "Low")
    high = get_value_from_prefs(prefs, "High")

    if broker == "Coinbase":
        broker_obj = Coinbase(currency, api_key, secret_key)

    trader_obj = USD_Trader(usd_per_buy, float(low), float(high), broker_obj)

    return broker_obj, trader_obj

def get_value_from_prefs(prefs_file, key):
    """Reads the next line, splits the text based on the key, and formats."""

    raw = prefs_file.readline().split(key + "=")
    return raw[1].rstrip()
