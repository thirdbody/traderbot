"""An exchange account; usually used with an API key."""

from abc import ABC  # Abstract base class

class Broker(ABC):
    """An exchange account; usually used with an API key."""

    def __init__(self, currency):

        if currency != "BTC" and currency != "ETH" and currency != "LTC":
            raise TypeError("Currency not supported")
        else:
            self.currency = currency

    def buy(self, amount):
        """Buys crypto (amount is in crypto)"""
        pass

    def sell(self, amount):
        """Sells crypto (amount is in crypto)"""
        pass

    def get_price(self):
        """Checks the price of 1 unit of self.currency with the broker."""
        pass

    def fiat_to_crypto(self, amount, fiat="USD"):
        """Converts amount of fiat into the desired crypto."""
        pass

    def crypto_to_fiat(self, amount, fiat="USD"):
        """Converts amount of crypto into desired fiat."""
        pass
