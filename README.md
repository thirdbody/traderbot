# traderbot

**NOTE: Coinbase API is broken. Not intending to fix. Repo is archived/deprecated.**

[![Build Status](https://travis-ci.org/Myl0g/traderbot.svg?branch=master)](https://travis-ci.org/Myl0g/traderbot)
[![PyPI version](https://badge.fury.io/py/traderbot.svg)](https://badge.fury.io/py/traderbot)
[![Maintainability](https://api.codeclimate.com/v1/badges/4dcb5d57d61d94aaf3b7/maintainability)](https://codeclimate.com/github/Myl0g/traderbot/maintainability)

**This project does not in any way endorse trading cryptocurrency. Please, for the love of all that is holy, understand what crypto is before trying to get rich. You will lose money otherwise, and probably still will even with knowledge.**

traderbot is a simple automatic cryptocurrency trader.

The aim of it is to automate the process of:

* buying crypto when the price is low
* selling crypto when the price is high

After all, we can't all stay up till 5 AM every night waiting for that precious price spike.

# Installation

Note: **trader.py is designed to be run 24/7. As such, it's best to run it on a Raspberry Pi running 24/7 or on a server running 24/7. Example server services are Linode and DigitalOcean.**

## Via Pip

`pip3 install traderbot`

Use `sudo` if necessary.

## Via Download

The easiest option is to [download the zip](https://github.com/Myl0g/trader.py/archive/master.zip) onto the desired device.

The other option is to use Git to clone the repo onto the computer. 

```
git clone https://github.com/Myl0g/traderbot
```

Optionally (*not recommended; makes uninstallation difficult*), you can install the download with its setup.py file with `python setup.py install --record program_locations.txt`. Please do not delete `program_locations.txt`; it provides the data needed to uninstall traderbot.

## Run the Program

If you installed with `pip3` or `python3 setup.py`, run the command `traderbot` to get started.

If you downloaded it, you can also run the software right from its folder:

```
cd path/to/traderbot
cd traderbot
chmod +x main.py
./main.py
```

# Usage

When you first run the app, you will be asked several questions about how the app should behave. If you change your mind after the fact, stop the app and edit the "trader-prefs" file in your home folder.

The app will refresh prices from your broker every 5 minutes.

# Uninstallation

Your preferences file is located in your home/User folder, and could contain sensitive information (e.g. API keys). Delete the file named `.trader-prefs` to remove all saved data.

---

If you used `pip` to install, run `pip3 uninstall traderbot` (use `sudo` if needed).

If you used `python3 setup.py install`, delete all files in the `program_locations.txt` to completely remove the program.

If you downloaded traderbot and have been running it right from the downloaded folder, simply delete the folder to remove the program.
